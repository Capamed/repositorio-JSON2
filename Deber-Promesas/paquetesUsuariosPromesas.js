const funcionCrearUsuarioPromesa = require('./crearUsuario.js')
const funcionBuscarUsuarioPromesa = require('./buscarUsuario.js')
const funcionEliminarPromesa= require('./eliminarUsuarios.js')


module.exports={
    funcionCrearUsuarioPromesa,
    funcionBuscarUsuarioPromesa,
    funcionEliminarPromesa
}