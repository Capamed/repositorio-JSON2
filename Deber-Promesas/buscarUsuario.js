module.exports =(usuarios,usuarioABuscar)=>{
    let usuarioEncontrado = usuarios.find((valor)=>{
        
        return valor.nombre === usuarioABuscar.nombre
    })
    return new Promise((resolve, reject)=>{
     if(usuarioEncontrado){
        resolve({
                usuarioEncontrado
            })
    }else{
        reject({
            mensaje: 'NO SE ENCONTRO USUARIO',
        })
    }
})
}