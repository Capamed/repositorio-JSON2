module.exports.misUsuarios = [
    {
        nombre: 'Edwin',
        apellido: 'Avila'
    },
    {
        nombre: 'Bryan',
        apellido: 'Avila'
    },
    {
        nombre: 'Andrea',
        apellido: 'Gudiño'
    },
]

module.exports.usuarioABuscarOCrear = {
    nombre: 'Javier',
    apellido: 'Washo'
}
module.exports.usuarioEliminarNoExiste = {
    nombre: 'Javier',
    apellido: 'Washo'
}

module.exports.usuarioEliminarSiExiste = {
    nombre: 'Edwin',
    apellido: 'Avila'
}

module.exports.parametroBusqueda = 'win'
module.exports.parametroBusquedaNoExiste = 'Ja'