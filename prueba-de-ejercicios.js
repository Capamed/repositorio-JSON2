var menu = require('console-menu');
var arregloUsuarios = ['Edwin Avila','Bryan Avila']
var arregloRestaurantes = ['CHIFA DORADO','DOÑA ROSA']
var arregloComidas = ['Chaulafan','Chop-Suey']
var contador = 0
var arregloAux =[]

const ejecutarMenuPrincipal = (verListaUsuarios) => {
  
    const menuSeleccion =[
        { hotkey: '1', title: 'Crear usuario', data: { nombre: 'CrearUsuario' } },
        { hotkey: '2', title: 'Logear usuario', data: { nombre: 'LogearUsuario' } },
        { hotkey: 'X', title: 'Salir', data:{nombre: 'Salir'}}
    ]
 
   if(verListaUsuarios === true){
        menuSeleccion[2]= { hotkey: '3', title: 'Listar Usuarios', data: { nombre: 'ListarUsuario' }}
        menuSeleccion[3]= {hotkey: '4', title: 'Regresar', data: { nombre: 'Regresar' }}
    }
    if(verListaUsuarios=== false){
        menuSeleccion[3]= { hotkey: 'R', title: 'Reporte', data: { nombre: 'Reporte' }}
    }
//CONSTANTEs DE CONFIGURACIONES PARA LOS MENUS:  
const configuracionesMenuPrincipal = {
    header: 'Menu Principal',
    border: true,
} 
const configuracionesMenuListaUsuarios = {
    header: 'Lista de Usuarios',
    border: true,
}
const configuracionesMenuPrincipalRestaurantes = {
    header: 'Menu Restaurantes',
    border: true,
}

const configuracionesVerListaRestaurantes = {
    header: 'Lista de Restaurantes',
    border: true,
}

const configuracionesMenuPrincipalComidas = {
    header: 'Menu Comidas',
    border: true,
}
const configuracionesVerListaComidas = {
    header: 'Lista de Comidas',
    border: true,
}
const configuracionesReporte = {
    header: 'REPORTE DEL SISTEMA',
    border: true,
}


//EJECUCIONES DEL LOS MENUS:
const ejecutarMenuListaUsuarios = (i)=>{
    
    const menuSeleccionUsuarios =[
        { hotkey: '1', title: arregloUsuarios[0], data: { nombre: 'PrimerNombre' } },
        { hotkey: '2', title: arregloUsuarios[1], data: { nombre: 'SegundoNombre' } },
    ]
    var edwin = menuSeleccionUsuarios.forEach(function(valor){
        return menu(valor)
    }); 
    console.log(menuSeleccionUsuarios.length)
    return menu(edwin,configuracionesMenuListaUsuarios).then((itemSeleccionadoUsuarios) => {
        if(itemSeleccionadoUsuarios.hotkey){
            arregloAux[i]=itemSeleccionadoUsuarios.title
            ejecutarMenuPrincipalRestaurantes(true,i)
        }
    })
}

//Restarantes
const ejecutarMenuPrincipalRestaurantes = (usuarioEscogido,i)=>{
    const menuPrincipalSeleccionRestaurante =[
        { hotkey: '1', title: 'Crear Restaurante', data: { nombre: 'CrearRestaurantes' } },
        { hotkey: '2', title: 'Ver Restaurantes', data: { nombre: 'VerRestaurantes' } },
    ]
    return menu(menuPrincipalSeleccionRestaurante,configuracionesMenuPrincipalRestaurantes).then((itemSeleccionadoRestaurantes) => {
                //if crear Restaurante
        if(itemSeleccionadoRestaurantes.hotkey === '2'){
            ejecutarMenuListaRestaurantes(true,i)
        }
    })
}
const ejecutarMenuListaRestaurantes = (verRestaurantes,i)=>{
    const menuSeleccionRestaurante =[
        { hotkey: '1', title: arregloRestaurantes[0], data: { nombre: 'Restaurante1' } },
        { hotkey: '2', title: arregloRestaurantes[1], data: { nombre: 'Restaurante2' } },
    ]
    return menu(menuSeleccionRestaurante,configuracionesVerListaRestaurantes).then((itemSeleccionadoRestaurante) => {
        if(itemSeleccionadoRestaurante.hotkey){
            arregloAux[i]+=' '+itemSeleccionadoRestaurante.title
            ejecutarMenuComidas(true,i) 
        } 
    })
}

//Comidas
const ejecutarMenuComidas = (comidaEscogida,i)=>{
    const menuPrincipalSeleccionComida =[
        { hotkey: '1', title: 'Crear Comida', data: { nombre: 'CrearComida' } },
        { hotkey: '2', title: 'Ver Comida', data: { nombre: 'VerComida' } },
    ]
    return menu(menuPrincipalSeleccionComida,configuracionesMenuPrincipalComidas).then((itemSeleccionadoComida) => {
                //if crear COMIDA
        if(itemSeleccionadoComida.hotkey === '2'){
            ejecutarListaComidas(true,i)
        }
    })
}

const ejecutarListaComidas = (verComida,i)=>{
    const menuSeleccionComida =[
        { hotkey: '1', title: arregloComidas[0], data: { nombre: 'Comida1' } },
        { hotkey: '2', title: arregloComidas[1], data: { nombre: 'Comida2' } },
    ]
    return menu(menuSeleccionComida,configuracionesVerListaComidas).then((itemSeleccionadoComida) => {
            arregloAux[i]+=' '+itemSeleccionadoComida.title 
            ejecutarMenuPrincipal(false)
    })
}


const ejecutarReporte = (arregloAux)=>{
    const reporte =[
        { hotkey: 'R', title: 'Regresar', data: { nombre: 'PrimerNombre' } },
    ]
    arregloAux.forEach((valor,indice) => {
        console.log(indice,valor)
    });
    return menu(reporte,configuracionesReporte).then((itemSeleccionado) => {
        if(itemSeleccionado.hotkey === 'R'){
        ejecutarMenuPrincipal(false)
    }
})
}



//RETURN GENERAL DEL MENU PRINCIPAL:
    return menu(menuSeleccion,configuracionesMenuPrincipal)
        .then((itemSeleccionado) => {
            switch (itemSeleccionado.hotkey) {
                case '1':
                    console.log('Escogió la Opcion 1 Crear Usuario');
                    ejecutarMenuPrincipal(true)
                    break;
                case '2':
                    contador+=1
                    console.log('Escogió la Opcion 2 Logear Usuario');
                    ejecutarMenuListaUsuarios(contador)
                    break;
                 case 'X':
                    console.log('Escogio la Opcion X Salir');
                    console.log('GRACIAS POR USAR NUESTRO SISTEMA')
                    break;
                case '4':
                    console.log('Escogio la Opcion 4 Regresar');
                    ejecutarMenuPrincipal()
                    break;
                case 'R':
                    console.log('Escogio la Opcion R Reporte');
                    ejecutarReporte(arregloAux)
                    break;
                default:
                    console.log('Opcion Invalida');
            }
        });
}
ejecutarMenuPrincipal()