//metodo para redondear 
var numero1 = 3.2    
var numero2 = 3.8
var numero3 = 0.2 
var numero4 = 0.9
var numero5 = 4.2231
var numero6 = 5.2927
var numero7 = -4.5
var numero8 = -4.8

console.log('Deja en la misma base'+ Math.round(numero1))
console.log('Si redondea'+Math.round(numero2))
console.log('Si redondea en los negativos no aplica el 0.5, pero 0.51 si'+Math.round(+numero7))
console.log('Si redondea'+Math.round(numero8))



// ceil, math
console.log('Siempre redondea al primero CEIL'+Math.round(numero1))

//ceil Negativos
console.log('CEIL negativo'+Math.ceil(numero7))

//floor: solo al entero sin imporatar los dcimales 
console.log('Positivo Floor'+Math.floor(numero1))
console.log('Negativo floor'+Math.floor(numero8))

//numeros aletorios 
console.log('Random', Math.random()*100)
console.log('Random', Math.round(Math.random()*100))
console.log('Random', Math.round(Math.random()*100))
console.log('Random', Math.round(Math.random()*100))
console.log('Random', Math.round(Math.random()*100))

//Pa ver el numero mas grande 

console.log(Math.max(3,5,7,9,10,-2))
console.log(Math.min(3,5,7,9,10,-2))
console.log(Math.max(...[3,5,7,9,10,-2]))