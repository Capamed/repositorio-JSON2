const Joi = require('joi');


const jsonPersona = {
    nombre: Joi.string().min(3).max(30).required(),
    apellido: Joi.string().min(3).max(30).required(),
    edad: Joi.number().integer().min(18).max(50), 
}

const jsonValidacionPersona = Joi.object().keys(jsonPersona).with('nombre','apellido')


const jsonPrueba ={
    nombre : 'Edwin',
    apellido: 'Avila'
}


Joi.validate(jsonPrueba, jsonValidacionPersona, (err, valor)=>{
    console.log(err,valor)
 });  