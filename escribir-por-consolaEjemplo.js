var readlineSync = require('readline-sync');
var menu = require('console-menu');

/*
  animals = ['Lion', 'Elephant', 'Crocodile', 'Giraffe', 'Hippo'],
  index = readlineSync.keyInSelect(animals, 'Which animal?');
console.log('Ok, ' + animals[index] + ' goes to your room.');
*/
/*var readlineSync = require('readline-sync');
var userName = readlineSync.question('May I have your name? ');
console.log('Hi ' + userName + '!'); */
/*var readlineSync = require('readline-sync'),
  MAX = 60, MIN = 0, value = 30, key;
console.log('\n\n' + (new Array(20)).join(' ') +
  '[Z] <- -> [X]  FIX: [SPACE]\n');
while (true) {
  console.log('\x1B[1A\x1B[K|' +
    (new Array(value + 1)).join('-') + 'O' +
    (new Array(MAX - value + 1)).join('-') + '| ' + value);
  key = readlineSync.keyIn('',
    {hideEchoBack: true, mask: '', limit: 'zx '});
  if (key === 'z') { if (value > MIN) { value--; } }
  else if (key === 'x') { if (value < MAX) { value++; } }
  else { break; }
}
console.log('\nA value the user requested: ' + value);
secret = readlineSync.question('Please whisper sweet words: ', {
    hideEchoBack: true,
    mask: require('chalk').magenta('\u2665')
  });*/


console.log('Bienvenidos');

console.log('Seleccione una opcion de menu');

const arregloUsuario = []



const registrarUsuario = function (datos) {
    console.log('Datos ', datos);
    arregloUsuario.push(datos);
    const hayUsuarios = arregloUsuario.length > 0

    if(hayUsuarios){
        ejecutarMenu(true)
    }else {
        ejecutarMenu()
    }
  
}

const logearUsuario = function (datos) {
    console.log('Datos ', datos);
    console.log(arregloUsuario);
    seleccionarUsuario(arregloUsuario)
}

const seleccionarUsuario = (usuarios)=>{
    const hayUsuarios = usuarios.length > 0

    if(hayUsuarios){
        ejecutarMenuUsuarios(usuarios)
    }else {
        ejecutarMenu()
    }
}

// permitir seleccionar un usuario con el menu si y solo si existen usuarios registrados listar todo slos usuarios registrados en el menu

const ejecutarMenu = (agregarOpcion) => {
    const menuSeleccion =[
        { hotkey: '1', title: 'Crear usuario', data: { nombre: 'Adrian' } },
        { hotkey: '2', title: 'Logear usuario', data: { nombre: 'Vicente' } }
    ]


    if(agregarOpcion){
        menuSeleccion[2]= { hotkey: '3', title: 'Listar usuarios', data: { nombre: 'Adrian' }}
    }


    return menu(menuSeleccion, {
            header: 'Menu',
            border: true,
        })
        .then(item => {
            switch (item.hotkey) {
                case '1':
                    console.log('Opcion 1');
                    registrarUsuario(item.data);
                    break;
                case '2':
                    console.log('Opcion 2');
                    logearUsuario(item.data);
                    break;
                    case '3':
                    console.log('Opcion 3');
                    ejecutarMenuUsuarios(arregloUsuario)
                    break;
                default:
                    console.log('Opcion invalida');
            }
        });
}
const ejecutarMenuUsuarios = (usuarios) => {

    let usuarios2=[]
   usuarios.forEach((valor,indice) => {
       let objetoMenu={
        hotkey:indice,
        title:valor.nombre
       }
       usuarios2.push(objetoMenu)
   });

   
    console.log('usuarios2', usuarios2)
    return menu(usuarios2, {
            header: 'Menu',
            border: true,
        })
        .then(item => {
            switch (item.hotkey) {
                case '1':
                    console.log('Opcion 1');
                    registrarUsuario(item.data);
                    
                    break;
                case '2':
                    console.log('Opcion 2');
                    logearUsuario(item.data);
                    break;
                default:
                    console.log('Opcion invalida');
            }
        });

}

ejecutarMenu();