const suma = require('./suma')
const resta = require('./resta')
const multiplicar = require('./multiplicar')
const dividir = require('./dividir')

module.exports = {
    suma,
    resta,
    multiplicar,
    dividir
}
