const paquetes = require('./paquetes-callbacksUsuarios.js')
module.exports = (arreglo,usarioAbuscarOCrear,cb)=>{
    paquetes.buscarUsuario(arreglo,usarioAbuscarOCrear,(respuestaDelCallbackDeBuscarUsuario)=>{
        if (respuestaDelCallbackDeBuscarUsuario.usuarioEncontrado) {
            cb({
                mensaje: 'se encontro el usuario',
                usuario: respuestaDelCallbackDeBuscarUsuario.usuarioEncontrado
            })
        } else {    
            paquetes.crearUsuario(arreglo,usarioAbuscarOCrear,(respuestaDelCallbackDeCrearUsuario)=>{
                cb({
                    mensaje: 'no se encontro pero se lo creo correctamente',
                    arreglo
                })
            })            
        }
    })

}
