const crearUsuario = require('./deber-crearusuario-callbacks.js')
const buscarUsuario = require('./deber-buscarusuario-callback.js')
const buscarOcrearUsuario = require('./deber-buscarOcrearUsuario-callbacks.js')

module.exports ={
    crearUsuario,
    buscarUsuario,
    buscarOcrearUsuario
}