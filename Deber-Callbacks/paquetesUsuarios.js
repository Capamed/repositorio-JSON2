const funcionCrearUsuarios = require('./crearUsuarios.js')
const funcionBuscarUsuarios = require('./buscarUsuarios.js')
const funcionBuscarOCrearUsuarios = require('./buscarOcrearUsuarios.js')
const funcionEliminarUsuario = require('./eliminarUnUsuario.js')
const funcionBuscarPorCaracter = require('./buscarPorCualquierCaracter.js')


module.exports={
    funcionCrearUsuarios,
    funcionBuscarUsuarios,
    funcionBuscarOCrearUsuarios,
    funcionEliminarUsuario,
    funcionBuscarPorCaracter
}