const paquetefuncionBuscarUsuario = require('./buscarUsuarios.js')

module.exports = (usuarios, nombreEliminar, cb) => {
    paquetefuncionBuscarUsuario.buscarUsuarios(usuarios, nombreEliminar, (respuestaDeBuscarUsuario) => {
        console.log(respuestaDeBuscarUsuario.usuarioEncontrado)
        if (respuestaDeBuscarUsuario.usuarioEncontrado) {
            let posicion = usuarios.indexOf(respuestaDeBuscarUsuario.usuarioEncontrado)
            usuarios.splice(posicion, 1)
            cb({
                mensaje: 'SE ELIMINO CON EXITO EL USUARIO',
                usuarios
            })
        } else {
            cb({
                mensaje: 'NO EXISTE USUARIO A ELIMINAR'
            })
        }
    })
}



