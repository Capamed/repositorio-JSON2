module.exports.buscarUsuarios = (usuarios,usuarioAbuscar,cb)=>{
    const usuarioEncontrado = usuarios.find((valor)=>{
        return valor.nombre === usuarioAbuscar.nombre && valor.apellido === usuarioAbuscar.apellido
    })
    if (usuarioEncontrado){
        cb({
            usuarioEncontrado,
        })
    }else{
        cb({
            mensaje: 'NO SE ENCONTRO USUARIO',
        })
    }
}
