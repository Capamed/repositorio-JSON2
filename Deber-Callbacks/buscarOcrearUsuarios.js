
const paquetefuncionBuscarUsuario = require('./buscarUsuarios.js')
const paquetefuncionCrearUsuario = require('./crearUsuarios.js')

module.exports = (usuarios,nombreUsuario,cb)=>{
    paquetefuncionBuscarUsuario.buscarUsuarios(usuarios,nombreUsuario,(respuestaDeBuscarUsuario)=>{
     if(respuestaDeBuscarUsuario.usuarioEncontrado){
            cb({
                mensaje: 'YA EXISTE EL USUARIO',
                respuestaDeBuscarUsuario
            })
      }else{
           paquetefuncionCrearUsuario.crearUsuarios(usuarios,nombreUsuario,(respuestaUsuarioCrear)=>{
           cb({
              respuestaUsuarioCrear
             })
        })  
     }
    })
}


