const saludo = require('./saludarPromesa.js')

function peticionUsuario() {
    saludo('HOLA').then(respuestaResolve=>{
        console.log(respuestaResolve)
    })
    .catch(respuestaReject=>{
        console.log(respuestaReject)
    })
}

peticionUsuario()