const objeto = {
    a:1,
    b:2,
    c:3,
    d:4
}
//EL mètodo objectKeysm sirve para poder editar os atributos del JSON
var arregloDelObjeto = Object.keys(objeto)
/* 
Object.defineProperty(objeto, 'a', {value:'NO TIENE'})
console.log(objeto)
Object.defineProperty(objeto, 'b', {value:'NO TIENE'})
Object.defineProperty(objeto, 'c', {value:'NO TIENE'})
Object.defineProperty(objeto, 'd', {value:'NO TIENE'})
console.log(objeto)
 */

arregloDelObjeto.map(valor=>{
Object.defineProperty(objeto,valor,{value:'NO TIENE'})
})

console.log(objeto)
///////////////////////////////////////////////////

var sumar =(a,b)=>{
const a1= a|0
const b1= b|0
    return a1+b1
}

console.log(sumar(5,))
// otra forma de usar un if en sola liena 
// valore por default
var sumar2 =(a=0,b=0)=>{
        return a+b
    }
    
    console.log(sumar2(5,))

//RE-ESTRUCTURACIÒN DEL OBJETO para imporat paquetes
const objeto2 = {
    nombre:'edwin',
    apellido:'Avila',
}
//como instanciarlo a las variables
const {nombre,apellido} = objeto2
console.log(nombre)
console.log(apellido)
